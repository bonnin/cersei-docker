from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

from accounts.models import Profile

class SignUpForm(UserCreationForm):
    
    class Meta:
        model = User
        fields = ["username", "password1", "password2"]

class ProfileForm(forms.ModelForm):
    ouid = forms.CharField(label="OUCU ID")

    class Meta:
        model = Profile
        fields = ['ouid']