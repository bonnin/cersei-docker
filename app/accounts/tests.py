from django.test import TestCase
from django.contrib.auth.models import User

from gnb.models import ActivityType

# Create your tests here.
class TestUserAccess(TestCase):
    def setUp(self):
        User.objects.create(password="pbkdf2_sha256$390000$nqT57IH9UCTgvgf1RSmnT5$f+CPxexZmuCMjqZaUgAu0ejR/nDi77CoZIJ+6BKqDrY=",
                username="bonnin",
                email="bonnin@loria.fr")
        

    def test_user_extists(self):
        user = User.objects.get(id=1)
        print(user.id)
        print(ActivityType._meta.object_name)