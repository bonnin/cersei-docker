from django.contrib.auth import login, authenticate
from django.shortcuts import redirect, render
from .forms import ProfileForm, SignUpForm

def signup(request):
    if request.method == 'POST':
        suform = SignUpForm(request.POST)
        pform = ProfileForm(request.POST)
        if suform.is_valid() and pform.is_valid():
            user = suform.save()
            profile = pform.save(commit=False)
            profile.user = user
            profile.save()
            raw_password = suform.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        suform = SignUpForm()
        pform = ProfileForm()
    return render(request, 'registration/signup.html', {'suform': suform, 'pform': pform})