from django.http import JsonResponse
from django.contrib.auth.models import User
from .models import Learning, Platform

def getActivities(request):
    is_ajax = request.headers.get('X-Requested-With') == 'XMLHttpRequest'
    if is_ajax and request.method == "GET":
        completed = request.GET["completed"] == "true"
        platform = Platform.objects.get(name = request.GET["platform"])
        user = User.objects.get(username = request.GET["username"])
        learningEvents = list(Learning.objects.filter(user = user, completed = completed, activity__platform = platform)
                            .values("id",
                                    "estimatedEffort",
                                    "predictedEffort",
                                    "perceivedEffort",
                                    "performance",
                                    "activity__type__image",
                                    "activity__name",
                                    "activity__url",
                                    "activity__description",
                                    "activity__image",
                                    "activity__module"))
        return JsonResponse({"activities": learningEvents}, status = 200)
    return JsonResponse({}, status = 400)

def postRating(request):
    is_ajax = request.headers.get('X-Requested-With') == 'XMLHttpRequest'
    if is_ajax and request.method == "POST":
        learningId = request.POST["learningId"]
        learning = Learning.objects.get(id = learningId)
        if learning.completed:
            learning.perceivedEffort = request.POST["effortRating"]
        else:
            learning.estimatedEffort = request.POST["effortRating"]
        learning.save()
        return JsonResponse({"response": "OK"}, status = 200)
    return JsonResponse({}, status = 400)

def removeRating(request):
    is_ajax = request.headers.get('X-Requested-With') == 'XMLHttpRequest'
    if is_ajax and request.method == "POST":
        learningId = request.POST["learningId"]
        learning = Learning.objects.get(id = learningId)
        learning.perceivedEffort = None
        learning.estimatedEffort = None
        learning.save()
        return JsonResponse({"response": "OK"}, status = 200)
    return JsonResponse({}, status = 400)