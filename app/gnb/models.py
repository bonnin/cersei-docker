from django.db import models
from django.contrib.auth.models import User

# A model for the different platform on which the activies are hosted
class Platform(models.Model):
    name = models.CharField(max_length=28)  # VLE or CERSEI

# A model for the different types of activities (quiz, forum, online exercise, tutored assessed material, etc.)
class Type(models.Model):
    name = models.CharField(max_length=28)
    image = models.CharField(max_length=496)

class Activity(models.Model):
    type = models.ForeignKey(Type, on_delete=models.CASCADE)  # quiz, forum, online exercise, etc.
    name = models.CharField(max_length=496)
    url = models.CharField(max_length=496)
    description = models.TextField()
    image = models.CharField(max_length=496)
    module = models.CharField(max_length=496)
    platform = models.ForeignKey(Platform, on_delete=models.CASCADE)

class Learning(models.Model):
    user =  models.ForeignKey(User, on_delete=models.CASCADE)
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE)
    completed = models.BooleanField()
    estimatedEffort = models.IntegerField(null=True)
    predictedEffort = models.IntegerField(null=True)
    perceivedEffort = models.IntegerField(null=True)
    performance = models.IntegerField(null=True)