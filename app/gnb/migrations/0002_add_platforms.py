from django.db import migrations
from gnb.models import Platform

def forwards_func(apps, schema_editor):
    vle = Platform(name='VLE')
    vle.save()
    cersei = Platform(name='CERSEI')
    cersei.save()

def reverse_func(apps, schema_editor):
    Platform.objects.filter(name='VLE').delete()
    Platform.objects.filter(name='CERSEI').delete()

class Migration(migrations.Migration):

    dependencies = [
        ('gnb', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
