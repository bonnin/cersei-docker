from accounts.models import Profile
from django.db import migrations
from gnb.models import Activity, Type, Learning, Platform
from django.contrib.auth.models import User

def forwards_func(apps, schema_editor):
    # Add a user named "jimi"
    user = User(username="jimi")
    user.set_password('jimijimi')
    user.save()
    Profile.objects.create(ouid="ggb65", user=user)

    # Types of activities
    quiz = Type.objects.get(name = "quiz")
    online = Type.objects.get(name = "online")
    reflection = Type.objects.get(name = "reflection")
    tma = Type.objects.get(name = "tma")

    # Add some VLE activities
    VLE = Platform.objects.get(name="VLE")
    vle1 = Activity.objects.create(type=tma, name="TMA01", url="https://learn2.open.ac.uk/course/view.php?id=213246&cmid=2023642", description="The first Tutored Marked Assignement from TT284.", module="TT284 - 22J - Web technologies", platform=VLE)
    vle2 = Activity.objects.create(type=tma, name="TMA02", url="https://learn2.open.ac.uk/course/view.php?id=213246&cmid=2023676", description="The second Tutored Marked Assignement from TT284.", module="TT284 - 22J - Web technologies", platform=VLE)
    vle3 = Activity.objects.create(type=tma, name="TMA03", url="https://learn2.open.ac.uk/course/view.php?id=213246&cmid=2023726", description="The third Tutored Marked Assignement from TT284.", module="TT284 - 22J - Web technologies", platform=VLE)

    # Add some CERSEI activities
    CERSEI = Platform.objects.get(name="CERSEI")
    cersei1 = Activity.objects.create(type=online, name="CA01 - CSS Pumpkin", url="/moodle/activities/ca01/", description="In this activity, you will construct a pumpkin part by part using CSS.", module="TT284 - 22J - Web technologies", platform=CERSEI)
    cersei2 = Activity.objects.create(type=online, name="CA02 - CSS piano", url="/moodle/activities/ca02/", description="In this activity, you will construct an interactive piano key by key using CSS.", module="TT284 - 22J - Web technologies", platform=CERSEI)
    cersei3 = Activity.objects.create(type=online, name="CA03 - Snake game", url="/moodle/activities/ca03/", description="In this activity, you will develop a complete Snake Game in JavaScript, in which the player can move a 5 cell long snake that can bite other snakes, or be bitten by other snakes who move randomly.", module="TT284 - 22J - Web technologies", platform=CERSEI)
    cersei4 = Activity.objects.create(type=online, name="CA04 - Learning to read with the Boscher method", url="/moodle/activities/ca04/", description="In this activity, you will develop a small PHP website on which young children can learn to read using the Boschert method, an old French method from the 50's.", module="TT284 - 22J - Web technologies", platform=CERSEI)

    # Add some entries for Jimi's learning
    Learning.objects.create(user=user, activity=vle1, completed=True, performance=50)
    Learning.objects.create(user=user, activity=vle2, completed=True, performance=60)
    Learning.objects.create(user=user, activity=vle3, completed=True, performance=70)
    Learning.objects.create(user=user, activity=cersei1, completed=False)
    Learning.objects.create(user=user, activity=cersei2, completed=False)
    Learning.objects.create(user=user, activity=cersei3, completed=False)
    Learning.objects.create(user=user, activity=cersei4, completed=False)

def reverse_func(apps, schema_editor):
    # Remove all learning entries of Jimi
    user = User.objects.get(username="jimi")
    Learning.objects.filter(user=user).delete()

    # Remove all CERSEI modules
    CERSEI = Platform.objects.get(name="CERSEI")
    Activity.objects.filter(platform=CERSEI).delete()

    # Remove all VLE modules
    VLE = Platform.objects.get(name="VLE")
    Activity.objects.filter(platform=VLE).delete()

    # Remove jimi
    Profile.objects.get(user=user).delete()
    user.delete()

class Migration(migrations.Migration):

    dependencies = [
        ('gnb', '0003_add_activity_types'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]