from django.db import migrations
from gnb.models import Type

def forwards_func(apps, schema_editor):
    Type.objects.create(name = "online", image = "images/online.svg")
    Type.objects.create(name = "quiz", image = "images/quiz.svg")
    Type.objects.create(name = "reflection", image = "images/reflection.svg")
    Type.objects.create(name = "tma", image = "images/tma.svg")

def reverse_func(apps, schema_editor):
    Type.objects.all().delete()

class Migration(migrations.Migration):

    dependencies = [
        ('gnb', '0002_add_platforms'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]