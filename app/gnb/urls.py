from django.urls import include, path

from gnb import views

urlpatterns = [
    path("get/ajax/activities/", views.getActivities, name = "get_activities"),
    path("post/ajax/post-rating/", views.postRating, name = "post_rating"),
    path("post/ajax/remove-rating/", views.removeRating, name = "remove_rating"),
]